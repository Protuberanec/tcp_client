#include "qtabclient.h"

void QTabClient::PlaceComponentsOnBox()
{
    grid_box->setSpacing(5);
    grid_box->addWidget(str_to_server, 0, 0);
    grid_box->addWidget(send_to_server, 0, 1);
    grid_box->addWidget(TableData, 1,0);
    grid_box->addLayout(vertical_box, 1, 1);

    vertical_box->addWidget(pb_disconnect);
    vertical_box->addWidget(pb_send_file);
    vertical_box->addWidget(CB_SaveToFile);
    vertical_box->addStretch(1);
}

void QTabClient::CreateTable()
{
    TableData = new QTableWidget();
    TableData->setColumnCount(3);
    QStringList hor_header;
    hor_header << "time" << "data" << "to/from";
    TableData->setHorizontalHeaderLabels(hor_header);
}

quint16 QTabClient::AddNewRow()
{
    TableData->setRowCount(TableData->rowCount() + 1);
    return TableData->rowCount() - 1;
}

void QTabClient::AddDataToTable(int row, int col, QString data, QColor color)
{
    QTableWidgetItem *new_item;
    new_item = new QTableWidgetItem(data);
    new_item->setBackgroundColor(color);
    TableData->setItem(row, col, new_item);
}

QTabClient::QTabClient(QWidget *wdgt, QObject *parent) : tcp_client(parent) /*: QObject(parent)*/
{
    grid_box = new QGridLayout();
    vertical_box = new QVBoxLayout();

    str_to_server = new QLineEdit();

    CreateTable();

    send_to_server = new QPushButton();
    send_to_server->setText("send data");
    connect(send_to_server, SIGNAL(clicked(bool)), this, SLOT(slot_SendDataToServer()));

    pb_disconnect = new QPushButton();
    pb_disconnect->setText("disconnect");
    connect(pb_disconnect, SIGNAL(clicked(bool)), this, SLOT(slot_DisconnectFromServer()));

    pb_send_file = new QPushButton();
    pb_send_file->setText("send file");

    CB_SaveToFile = new QCheckBox();
    CB_SaveToFile->setText("save to file");
    CB_SaveToFile->setChecked(false);

    PlaceComponentsOnBox();

    dialog_FileSend = new QFileDialog();
    connect(pb_send_file, SIGNAL(clicked()), this, SLOT(slot_OpenFileToSendSever()));

    if (wdgt == nullptr)
        return;

    wdgt->setLayout(grid_box);
    wdgt->show();

    connect(this, SIGNAL(sig_NotConnected()), this, SIGNAL(sig_CloseTab()));
    connect(this, SIGNAL(sig_NewData(QByteArray)), this, SLOT(slot_ShowNewData(QByteArray)));
}

void QTabClient::slot_OpenFileToSendSever()
{
    QString file_name = dialog_FileSend->getOpenFileName();

    if (file_name.size() == 0) {
        return;
    }

    QTime curr_time = QTime::currentTime();
    quint16 num_row = AddNewRow();
    QTableWidgetItem *new_item;

    QFile data_file(file_name);
    if (data_file.open(QIODevice::ReadOnly) == false) {
        AddDataToTable(num_row, 0, curr_time.toString("HH:mm:ss:zzz"), QColor(0xFF, 0x00, 0x80));
        AddDataToTable(num_row, 0, "can't open file", QColor(0xFF, 0x00, 0x80));
        return;
    }

    QByteArray data_from_file = data_file.readAll();
    data_file.close();

    AddDataToTable(num_row, 0, curr_time.toString("HH:mm:ss:zzz"), QColor(0x0F0, 0xf0, 0x80));
    AddDataToTable(num_row, 1, "start send file", QColor(0x0F0, 0xf0, 0x80));
    AddDataToTable(num_row, 2, "to", QColor(0x0F0, 0xf0, 0x80));

    slot_SendToServer(data_from_file);
}

void QTabClient::slot_SendDataToServer()
{
    slot_SendToServer(str_to_server->text().toUtf8());

    QTime curr_time = QTime::currentTime();
    quint16 num_row = AddNewRow();

    AddDataToTable(num_row, 0, curr_time.toString("HH:mm:ss:zzz"), QColor(0x50, 0xf0, 0x80));
    AddDataToTable(num_row, 1, str_to_server->text(), QColor(0x50, 0xf0, 0x80));
    AddDataToTable(num_row, 2, "to", QColor(0x50, 0xf0, 0x80));
}

void QTabClient::slot_DisconnectFromServer()
{
    disconnect_from_server();
    emit sig_CloseTab();
}

void QTabClient::slot_ShowNewData(QByteArray data_from_server)
{
    QTime curr_time = QTime::currentTime();
    quint16 num_row = AddNewRow();

    AddDataToTable(num_row, 0, curr_time.toString("HH:mm:ss:zzz"), QColor(0x9F, 0xFF, 0xFF));
    AddDataToTable(num_row, 1, data_from_server.data(), QColor(0x9F, 0xFF, 0xFF));
    AddDataToTable(num_row, 2, "from", QColor(0x9F, 0xFF, 0xFF));
}



