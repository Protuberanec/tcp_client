#include "tcp_client.h"

quint8 tcp_client::getStatusConnection() const
{
    return status_connection;
}

tcp_client::tcp_client(QObject *parent) : QObject(parent)
{
    client = new QTcpSocket();
    connect(client, SIGNAL(connected()), this, SLOT(slot_Connected()));
    connect(client, SIGNAL(readyRead()), this, SLOT(slot_ReadyRead()));
    connect(client, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(slot_Error(QAbstractSocket::SocketError)));
    connect(client, SIGNAL(disconnected()), this, SLOT(slot_Disconnected()));
}

quint8 tcp_client::connect_to_server(QString ip, quint16 port)
{
    if (status_connection != st_cl_connected) {
        client->connectToHost(ip, port);
        bool isConnected = client->waitForConnected(10);
        qDebug() << "connection to gen server is " << isConnected;
        return isConnected == true ? st_cl_connected : st_cl_disconnected;
    }
    return st_cl_connected;
}

quint8 tcp_client::disconnect_from_server()
{
    client->disconnectFromHost();
    return 0;
}

void tcp_client::slot_Connected()
{
    status_connection = st_cl_connected;
    emit sig_ConnectionEstabled();
}

void tcp_client::slot_ReadyRead()
{
//    QDataStream in(client);
//    in.setVersion(QDataStream::QT_4_5);
    QByteArray data_from_server;

    data_from_server = client->readAll();
//    qDebug() << "data: " << data_from_server;
    emit sig_NewData(data_from_server);
}

void tcp_client::slot_Error(QAbstractSocket::SocketError error)
{
    switch(error) {
        case QAbstractSocket::ConnectionRefusedError :
        break;
        case QAbstractSocket::RemoteHostClosedError :
        break;
        case QAbstractSocket::HostNotFoundError :
        break;
        case QAbstractSocket::OperationError :
        break;

        default:    //to look more errors, need to remove default, and final compilators will show you additional posible errors...
        break;
    }
#ifdef SAVE_LOG
    qDebug() << tr("error %1").arg(error);
#endif
    status_connection = st_cl_disconnected;
//    emit sig_ErrorConnection(error);
}

void tcp_client::slot_SendToServer(QByteArray data)
{
    QAbstractSocket::SocketState state_client = client->state();

    if (state_client != QAbstractSocket::ConnectedState) {
#ifdef SAVE_LOG
        qDebug() << "connection error..." << state_client;
#endif
        status_connection = st_cl_disconnected;
        emit sig_ErrorConnection(state_client);
        return;
    }

    qint64 count_byte = client->write(data);
    if (count_byte != data.size()) {
        qDebug() << "hm error in send data to server...";
    }
}

void tcp_client::slot_Disconnected()
{
    qDebug() << "client were disconnected";
    emit sig_NotConnected();
}
