#ifndef TCP_CLIENT_H
#define TCP_CLIENT_H

#include <QObject>
#include    <QDebug>
#include    <QTcpSocket>
#include    <QIODevice>
#include <QProcess>

enum STATUS_CLIENT_CONNECTION {
    st_cl_connected = 0,
    st_cl_disconnected = 1,
};

class tcp_client : public QObject
{
    Q_OBJECT
private:
    QTcpSocket *client;
    quint8 status_connection;
public:
    explicit tcp_client(QObject *parent = nullptr);

    quint8 connect_to_server(QString, quint16);
    quint8 disconnect_from_server();

    quint8 getStatusConnection() const;

signals:
    void sig_NotConnected();
    void sig_NewData(QByteArray);   //signal to send get data from server   //connect it...

    void sig_ErrorConnection(QAbstractSocket::SocketState);

    void sig_ConnectionEstabled();

public slots:
    void slot_Connected();
    void slot_ReadyRead();
    void slot_Error(QAbstractSocket::SocketError);
    void slot_SendToServer(QByteArray); //slot to send data to server   //connect it
    void slot_Disconnected();
};

#endif // TCP_CLIENT_H
