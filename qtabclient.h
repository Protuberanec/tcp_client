#ifndef QTABCLIENT_H
#define QTABCLIENT_H

#include    <QWidget>
#include    <QObject>
#include    <QDebug>
#include    <QLineEdit>
#include    <QPushButton>
#include    <QTableWidget>
#include    <QTableWidgetItem>
#include    <QGridLayout>
#include    <QVBoxLayout>
#include    <QFileDialog>
#include    <QCheckBox>
#include    <QTime>
#include    <QFile>

#include "tcp_client.h"

class QTabClient : public tcp_client
{
    Q_OBJECT
private :
    QGridLayout *grid_box;
    QVBoxLayout *vertical_box;
    QLineEdit *str_to_server;
    QTableWidget *TableData;
    QPushButton *send_to_server;
    QPushButton *pb_disconnect;
    QPushButton *pb_send_file;
    QFileDialog *dialog_FileSend;
    QCheckBox *CB_SaveToFile;


    void PlaceComponentsOnBox();
    void CreateTable();
    quint16 AddNewRow();
    void AddDataToTable(int row, int col, QString data, QColor color);

public:
    explicit QTabClient(QWidget *wdgt = nullptr, QObject *parent = nullptr);

signals:
    void sig_CloseTab();

public slots:
    void slot_OpenFileToSendSever();
    void slot_SendDataToServer();
    void slot_DisconnectFromServer();
    void slot_ShowNewData(QByteArray);
};

#endif // QTABCLIENT_H
