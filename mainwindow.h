#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "qtabclient.h"

namespace Ui {
class MainWindow;
}

#define FILE_NAME_PORT_NUM  "port.ini"

class MainWindow : public QMainWindow
{
    Q_OBJECT
private :

    quint16 default_port {15000};
    QFile temp_file_cmd; //for future, write cmd to file... and when application is start, create drop-down list
    QList<QTabClient*> tab_clients;

    void GetDefaultPortNum();
    void UpdateDefaultPort(quint16);

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

private slots :
    void slot_CreateTab();
    void slot_CloseTab();
};

#endif // MAINWINDOW_H
