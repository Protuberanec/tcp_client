#include "mainwindow.h"
#include "ui_mainwindow.h"

void MainWindow::GetDefaultPortNum()
{
    QFile temp_file_param(FILE_NAME_PORT_NUM);
    if (temp_file_param.exists() == false) {
        temp_file_param.open(QIODevice::WriteOnly);
        temp_file_param.write("15000");
        temp_file_param.close();
    }
    else {
        temp_file_param.open(QIODevice::ReadOnly);
        QByteArray temp_port = temp_file_param.readAll();
        bool ok = false;
        default_port = temp_port.toUInt(&ok, 10);
        temp_file_param.close();
    }

    ui->LE_PortNum->setText(QString::number(default_port));
}

void MainWindow::UpdateDefaultPort(quint16 port_num)
{
    default_port = port_num;
    QFile temp_file_param(FILE_NAME_PORT_NUM);
    temp_file_param.open(QIODevice::WriteOnly);
    char temp_buffer[8];
    memset(temp_buffer, 0x00, 8 * sizeof(char));
    temp_file_param.write(itoa(port_num, temp_buffer, 10));
    temp_file_param.close();
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(ui->PB_ConnectToServer, SIGNAL(clicked(bool)), this, SLOT(slot_CreateTab()));

    GetDefaultPortNum();
}

MainWindow::~MainWindow()
{
    UpdateDefaultPort(ui->LE_PortNum->text().toUInt());

    delete ui;
}

void MainWindow::slot_CreateTab()
{
    static quint8 count = 0;
    QWidget *tab = new QWidget;

    QTabClient *temp_tab_clients = new QTabClient(tab);
    tab_clients.append(temp_tab_clients);

    ui->tabWidget->addTab(tab, tr("%1:%2").arg(ui->LE_IpAddress->text()).arg(ui->LE_PortNum->text()));

    bool ok;
    quint16 port_num = ui->LE_PortNum->text().toUInt(&ok);
    tab_clients.last()->connect_to_server(ui->LE_IpAddress->text(), port_num);


    connect(tab_clients.last(), SIGNAL(sig_CloseTab()), this, SLOT(slot_CloseTab()));

    count++;
}

void MainWindow::slot_CloseTab()
{
    QTabClient *pointer = (QTabClient*)sender();
    int i = 0;
    for (; i < tab_clients.size(); i++) {
        if (tab_clients.at(i) == pointer) {
            ui->tabWidget->removeTab(i);
            delete tab_clients.at(i);
            tab_clients.removeAt(i);
            break;
        }
    }
}
